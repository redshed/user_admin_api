/**
 * Created by dvicente@solidgear.es on 10/11/2017
 */
'use strict'

const winston = require('winston');
require('winston-papertrail').Papertrail;
const uuid = require('uuid');
const contextService = require('request-context');
// const config = require('config');
//const env = config.get("label");
const os = require('os');

//console.log("logging env " + env);
const env = process.env.NODE_ENV;
console.log("NODE_ENV=" + process.env.NODE_ENV);

let hostLabel = "";
if (process.env.GOOGLE_CLOUD_PROJECT !== undefined) {
    hostLabel = "gae-" + process.env.GOOGLE_CLOUD_PROJECT + "-" + process.env.GAE_SERVICE;
}
else {
    hostLabel = os.hostname();
}

//LOGGING
//set console for local development
//papertrail for the rest
let winston_transports =[
    new winston.transports.Papertrail({
        host: 'logs4.papertrailapp.com',
        port: 25947,
        program: 'userdata',
        colorize: true,
        level: 'debug',
        hostname: hostLabel
    })];

if (env === "development") {
    winston_transports = [new winston.transports.Console({
        colorize: true,
        level: 'debug'
    })];
}

const winstonLogger = new winston.Logger({
    transports: winston_transports,
    exitOnError: false
});

winston.configure({
    transports: winston_transports,
    exceptionHandlers: winston_transports
});

winstonLogger.stream = {
    write: function (message, encoding) {
        winstonLogger.info(message);
    }
};

// Wrap Winston logger to print reqId in each log
const formatMessage = function(message) {
    let request_token = getToken();
    message = request_token ? request_token + " " + message : message;
    return message;
};

const setToken = (tag) => {
    contextService.set("request:token", tag + "-" + uuid.v4().replace(/-/g, "").substring(0,16));
};

const getToken = () => {
    return contextService.get("request:token");
};

const logger = {
    log: function(level, message) {
        winstonLogger.log(level, formatMessage(message));
    },
    error: function(message) {
        winstonLogger.error(formatMessage(message));
    },
    warn: function(message) {
        winstonLogger.warn(formatMessage(message));
    },
    verbose: function(message) {
        winstonLogger.verbose(formatMessage(message));
    },
    info: function(message) {
        winstonLogger.info(formatMessage(message));
    },
    debug: function(message) {
        winstonLogger.debug(formatMessage(message));
    },
    silly: function(message) {
        winstonLogger.silly(formatMessage(message));
    },
    getToken: getToken,
    setToken: setToken
};




module.exports = {logger, winston_transports, setToken, getToken};
