class AuthDetail { //maps to firebase user properties in firebase auth
    constructor() {
        this.disabled = false;
        this.displayName = null;
        this.email = null;
        this.emailVerified = false;
        this.password = null;
        this.phoneNumber = null;
        this.photoURL = null;
    }
}

class Company {
    constructor() {
        this.name = "";
        this.containerUrl = "";
        this.suppressTurnoverRate = false;
        this.farmSubscription = false;
        this.contacts = [];
    }
}

class Office {
    constructor() {
        this.companyId = null;
        this.name = "";
        this.state = "";
        this.customerServicePhone = null;
        this.disabled = false;
        this.orderTitleEmail = null;
        this.orderTitleEnabled = true;
        this.copyRepOnOrderTitle = false;
        this.defaultFarmCredits = 0;
        this.defaultProfileCredits = 0;
        this.enabledProducts = {
            farmTools: false,
            walkingFarm: false,
            calculators: false
        };
        this.officeContactUserIds = [];
    }
}

class Referral {
    constructor() {
        this.refCode = "";
        this.userId = "";
        this.referralUserId = "";
        this.accepted = false;
    }
}

class RepDetail {
    constructor() {
        this.repCode = "";
        this.defaultProfileCredits = 0;
        this.defaultFarmCredits = 0;
        this.defaultDocumentCredits = 0;
    }
}

class UserBase {
    constructor() {
        this.userId = "";
        this.authDetail = new AuthDetail();
        this.userDetail = new UserDetail();
        this.repDetail = new RepDetail();
        this.userCredits = new UserCredits();
    }
}

class UserCredits {
    constructor() {
        this.allocatedProfileCredits = 0;
        this.allocatedFarmCredits = 0;
        this.allocatedDocumentCredits = 0;
        this.remainingProfileCredits = 0;
        this.remainingFarmCredits = 0;
        this.remainingDocumentCredits = 0;
    }
}

class UserDetail {
    constructor() {
        this.userType = "";
        this.referralUserId = "";
        this.companyId = "";
        this.officeId = "";
        this.firstName = "";
        this.lastName = "";
        this.agentPhotoUrl = "";
        this.company = "";
    }
}

const UserTypes = {
    Admin: "Admin",
    OfficeAdmin: "OfficeAdmin",
    TitleRep: "TitleRep",
    Realtor: "Realtor"
};

module.exports = {AuthDetail, Company, Office, Referral, RepDetail, UserBase, UserCredits, UserDetail, UserTypes};
