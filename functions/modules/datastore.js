const _ = require("lodash");
const {AuthDetail, Company, Referral, RepDetail, UserBase, UserCredits, UserDetail, UserTypes} = require('./dataClasses');

//intialize admin and firestore
//firebase
const admin = require("firebase-admin");
const detailLogger = require('../detail_logger.js').logger;

const serviceAccount = require("./admin-dev-82b6b-firebase-adminsdk-d1wzc-e7b015c094.json");
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://admin-dev-82b6b.firebaseio.com"
});
const auth = admin.auth;
const firestore = admin.firestore();
const settings = { timestampsInSnapshots: true };
firestore.settings(settings);

class firebaseDatastore {
    constructor(logger, db, auth) {
        this.logger = logger;
        this.db = db;
        this.auth = auth;
    }

    //users
    async getUser(userId) {
        if (userId === undefined) {
            return {};
        }
        let user = new UserBase();
        let authUser = await this.auth().getUser(userId);
        let authDetail = authUser;
        let userDetail = await this.getUserDetail(userId);
        let repDetail = await this.getRepDetail(userId);
        let userCredits = await this.getUserCredits(userId);
        user.userId = authDetail.uid;
        user.authDetail = authDetail;
        user.userDetail = userDetail;
        user.repDetail = repDetail;
        user.userCredits = userCredits;
        return user;
    }

    async saveUser(userId, authDetail, userDetail, repDetail, userCredits) {
        // if empty userId, create user
        let authUserId = null;
        if (userId === undefined || userId === null) {
            let authUserResponse = await this.auth()
                .createUser(authDetail)
                .catch(err => {
                    this.logger.error(err.message);
                    throw(err.message);
                });
            authUserId = authUserResponse.uid;
        } else {
            let u = await this.auth().updateUser(userId, authDetail);
            authUserId = userId;
        }

        if (userDetail !== undefined && typeof userDetail === "object") {
            let userDetailResponse = await this.saveUserDetail(authUserId, userDetail);
        }

        if (repDetail !== undefined && typeof repDetail === "object") {
            let repDetailResponse = await this.saveRepDetail(authUserId, repDetail);
        }

        if (userCredits !== undefined && typeof userCredits === "object") {
            let userCreditResponse = await this.saveUserCredits(authUserId, userCredits);
        }

        return this.getUser(authUserId);
    }

    async getUserDetail(userId) {
        let userDetail = await this.db
            .collection("userDetail")
            .doc(userId)
            .get();
        let data = userDetail.data();
        let d = new UserDetail();
        if (data !== undefined) {
            d.userType = data.userType;
            d.referralUserId = data.referralUserId;
            d.companyId = data.companyId;
            d.officeId = data.officeId;
            d.firstName = data.firstName;
            d.lastName = data.lastName;
            d.agentPhotoUrl = data.agentPhotoUrl;
            d.suspended = (data.hasOwnProperty("suspended")) ? data.suspended : false;
        }
        return d;
    }

    async saveUserDetail(userId, detail) {
        let userDetail = await this.db
            .collection("userDetail")
            .doc(userId)
            .set(detail);
        return userDetail;
    }

    //reps
    async getUserCountForRep(repUserId) {
        //get all the reps in an office, then get all the users for those reps
        let count = {repCount: null, userCount: null};
        //reps by office
        const userDetailSnapshot = await this.db.collection("userDetail")
            .where("referral_user_id", "==", repUserId)
            .where("user_type", "==", 'Realtor')
            .get();
        let userDetails = userDetailSnapshot.docs.map((snap) => {
            let d = snap.data();
            d.id = snap.id;
            return d;
        });
        return userDetails.length;
    };

    async listUsers(userType) {
        let userList = [];
        let userDetailSnapshot = null;
        if (userType !== null) {
            userDetailSnapshot = await this.db.collection("userDetail")
                .where("userType", "==", userType)
                .get();

        } else {
            userDetailSnapshot = await this.db.collection("userDetail")
                .get();
        }
        let userDetailMap = await userDetailSnapshot.docs.map((snap) => {
            let userDetailFromSnap = snap.data();
            userDetailFromSnap.id = snap.id;
            return userDetailFromSnap;
        });
        for (let userDetailDoc of userDetailMap) {
            let user = await this.getUser(userDetailDoc.id);
            //user.userDetail = userDetailDoc;
            if (userType === UserTypes.TitleRep) {
                //inject user count
                user.RepUserCount = await this.getUserCountForRep(userDetailDoc.id);
            }
            userList.push(user);
        }
        return userList;
    }

    async getRepDetail(userId) {
        let repDetail = await this.db
            .collection("repDetail")
            .doc(userId)
            .get();
        let data = repDetail.data();
        let r = new RepDetail();
        if (data !== undefined) {
            r.repCode = data.repCode;
            r.defaultProfileCredits = data.defaultProfileCredits;
            r.defaultFarmCredits = data.defaultFarmCredits;
            r.defaultDocumentCredits = data.defaultDocumentCredits;
        }
        return r;
    }

    async saveRepDetail(userId, detail) {
        let repDetail = await this.db
            .collection("repDetail")
            .doc(userId)
            .set(detail);
        return repDetail;
    }

    async getUserCredits(userId) {
        let userCredits = await this.db
            .collection("userCredits")
            .doc(userId)
            .get();
        let data = userCredits.data();
        let r = null;
        if (data !== undefined) {
            r = new UserCredits({
                allocatedProfileCredits: data.allocatedProfileCredits,
                allocatedFarmCredits: data.allocatedFarmCredits,
                allocatedDocumentCredits: data.allocatedDocumentCredits,
                remainingProfileCredits: data.remainingProfileCredits,
                remainingFarmCredits: data.remainingFarmCredits,
                remainingDocumentCredits: data.remainingDocumentCredits
            });
        }
        else {
            r = new UserCredits({
                allocatedProfileCredits: 0,
                allocatedFarmCredits: 0,
                allocatedDocumentCredits: 0,
                remainingProfileCredits: 0,
                remainingFarmCredits: 0,
                remainingDocumentCredits: 0
            });
        }
        return r;
    }

    async saveUserCredits(userId, detail) {
        //todo: add validation
        let userCredits = await this.db
            .collection("userCredits")
            .doc(userId)
            .set(detail);
        return detail;
    }

    async saveReferral(refCode, referralUserId, userId) {
        let referral = new Referral();
        referral.refCode = refCode;
        referral.referralUserId = referralUserId;
        referral.userId = userId;

        let r = await this.db
            .collection("referral")
            .doc(refCode)
            .set({referralUserId: referral.referralUserId, userId: referral.userId});
        return referral;
    }

    async validateRefCode(refCode) {
        let q = await this.db
            .collection("referral")
            .doc(refCode)
            .get();
        return !q.exists; //doc should not exist
    }

    async getReferral(refCode) {
        let d = await this.db
            .collection("referral")
            .doc(refCode)
            .get();
        let referral = new Referral();
        referral.refCode = d.id;
        referral.referralUserId = d.data().referralUserId;
        referral.userId = d.data().userId;
        return referral;
    }

    async findReferral(referralUserId, userId) {
        let doc = undefined;
        await this.db
            .collection("referral")
            .where("userId", "==", userId)
            .where("referralUserId", "==", referralUserId)
            .get()
            .then((qs) => {
                if (qs.docs.length > 0) {
                    doc = qs.docs[0].data();
                    doc.id = qs.docs[0].id;
                }
            });

        if (doc !== undefined) {
            let referral = new Referral();
            referral.refCode = doc.id;
            referral.referralUserId = doc.referralUserId;
            referral.userId = doc.userId;
            return referral;
        }
        else {
            return null;
        }
    }

    //offices
    async listOffices( officeIdFilter ) {
        let officeList = [];
        let docSnapshot = await this.db
            .collection("office")
            .get();
        let offices = docSnapshot.docs.map((snap) => {
            return snap.data()
        });
        for (let office of offices) {
            let count = await this.getRepUserCount(office.id);
            office.RepUserCount = count;
            officeList.push(office);
        }
        return officeList;
    }

    async getOffice( officeId ) {
        let office = await this.db
            .collection("office")
            .doc(officeId)
            .get()
            .catch(err => {
                this.logger.error(err.message);
            });
        return office.data();
    };

    async saveOffice( office ) {
        //create
        let officeLookup = null;
        if (!office.id) {
            officeLookup = await this.db
                .collection("office")
                .add(office)
                .catch(err => {
                    this.logger.error(err.message);
                });
        }
        //update
        else {
            officeLookup = await this.db
                .collection("office")
                .doc(office.id)
                .set(office)
                .catch(err => {
                    this.logger.error(err.message);
                });
        }
        if (officeLookup !== null && officeLookup.id !== null) {
            return await this.getOffice(officeLookup.id);
        }
        else {
            return null;
        }
    }

    async deleteOffice( office ) {
        let r = await req.db
            .collection("office")
            .doc(officeId)
            .delete()
            .catch(err => {
                this.logger.error(err.message);;
            });
        return r;
    }

    //office additional methods
    //rep and user count by office
    async getRepUserCount( officeId ) {
        //get all the reps in an office, then get all the users for those reps
        let count = {repCount: null, userCount: null};
        //reps by office
        const repsByOfficeSnapshot = await this.db.collection("userDetail")
            .where("office_id", "==", officeId)
            .where("user_type", "==", 'TitleRep')
            .get();
        let repsDetails = await repsByOfficeSnapshot.docs.map((snap) => {
            let repFromSnap = snap.data();
            repFromSnap.id = snap.id;
            return repFromSnap;
        });

        let repList = [];
        let userList = [];

        for (repDetail of repsDetails) {
            let rep_user_id = repDetail.id;  //user_id for rep
            repList.push(rep_user_id);
            //users by rep
            const userDetailSnapshot = await this.db.collection("userDetail")
                .where("referral_user_id", "==", rep_user_id)
                .get();
            const userDetails = userDetailSnapshot.docs.map((snap) => {
                return snap.data()
            });
            for (let userDoc of userDetails) {
                userList.push(userDoc.id);
            }
        }
        count.repCount = repList.length;
        count.userCount = userList.length;
        return count;
    };

    //company
    async listCompanies() {
        let companyList = [];
        let resp = await this.db
            .collection("company")
            .get()
            .then(docs => {
                docs.forEach(doc => {
                    let c = doc.data();
                    c.id = doc.id;
                    companyList.push(c);
                });
            })
            .catch(err => {
                this.logger.error(err.message);
            });
        return companyList;
    }

    async getCompany( companyId ) {
        let company = null;
        if (companyId != null) {
            let companyResponse = await this.db
                .collection("company")
                .doc(companyId)
                .get()
                .catch(err => {
                    this.logger.error(err.message);
                });
            //get users in contact list
            company = companyResponse.data();
        }
        return company;
    }

    async saveCompany( company ) {
        //create
        let companyLookup = null;
        if (!company.id) {
            companyLookup = await this.db
                .collection("company")
                .add(company)
                .catch(err => {
                    this.logger.error(err.message);
                });
        }
        //update
        else {
            companyLookup = await this.db
                .collection("company")
                .doc(company.id)
                .set(company)
                .catch(err => {
                    this.logger.error(err.message);

                });
        }
        if (companyLookup !== null && companyLookup.id !== null) {
            return await this.getCompany(companyLookup.id);
        }
        else {
            return null;
        }
    }
}

const datastore = new firebaseDatastore(detailLogger, firestore, auth);

module.exports = { datastore, UserTypes };
