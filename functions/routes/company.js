const express = require("express");
const router = express.Router();
const listCompanies = require("../interfaces/company").listCompanies;
const getCompany = require("../interfaces/company").getCompany;
const saveCompany = require("../interfaces/company").saveCompany;

//list all
router.get("/", async (req, res, next) => {
  let companyList = await listCompanies();
  res.status(200).json(companyList);
});

router.get("/:companyId", async (req, res, next) => {
  let companyId = req.params.companyId;
  return getCompany(companyId);
});

//TODO: update these
router.post("/", async function(req, res, next) {
  let company = req.body;
  let companyResponse = await saveCompany( company );
  res.status(200).json(companyResponse);
});

router.patch("/:companyId", async function(req, res, next) {
  let company = req.body;
  let companyResponse = await saveCompany( company );
  res.status(200).json(companyResponse);
});

module.exports = router;
