const express = require("express");
const router = express.Router();
const _ = require("lodash");

const findUser = require('../interfaces/user').findUser;
const saveUser = require("../interfaces/user").saveUser;
const UserCredits = require("../interfaces/user").UserCredits;

const ds = require("../modules/datastore").datastore;
/**
 * Get user by userId
 * @route GET /user/:userId
 * @group User Management - Operations for User management
 * @param {string} userId.query.required - userId
 * @returns {object} 200 - User model
 * @returns {Error}  default - Unexpected error
 */
router.get("/:userId", async (req, res, next) => {
  let userId = req.params.userId;
  let user = await findUser(userId);
  res.status(200).json(user);
});


/**
 * Create User
 * @route POST /user
 * @group User Management - Operations for User management
 * @param {string} userId.query.required - userId
 * @returns {string} 200 - User created message
 * @returns {Error}  default - Unexpected error
 */
router.post("/", async (req, res, next) => {
  let userRequest = req.body;
  let user = await saveUser(
      null,
      userRequest
  ).catch((e) => {
      res.status(500).json({ message: e });
  });
  if (user) {
      res.status(200).json(user);
  }
});

/**
 * Update User
 * @route PATCH /user/:userId
 * @group User Management - Operations for User management
 * @param {string} userId.query.required - userId
 * @returns {string} 200 - User created message
 * @returns {Error}  default - Unexpected error
 */
router.patch("/:userId", async (req, res, next) => {
  let userId = req.params.userId;
  //should map to userBase - validate?
  let userRequest = req.body;
  //map firebase auth user dat

  //update userDetail
  let user = await saveUser(
    userId,
    userRequest
  );

  res.status(200).json(user);
});

/**
 * Delete User
 * @route Delete /user/:userId
 * @group User Management - Operations for User management
 * @param {string} userId.query.required - userId
 * @returns {string} 200 - User deleted message
 * @returns {Error}  default - Unexpected error
 */
router.delete("/:userId", async (req, res, next) => {
  let userId = req.params.userId;
  req.logger.info(userId);
  let user = await ds.deleteUser(userId);
  res.status(200).json("User " + userId + " deleted");
});

/**
 * Update User Credits - all in one
 * @route Patch /user/:userId/credits/update
 * @group User Credit Management - Operations for User Credit Management
 * @param {string} userId.query.required - userId
 * @returns {string} 200 -
 * @returns {Error}  default - Unexpected error
 */
router.patch("/:userId/credits/update", async (req, res, next) => {
    let userId = req.params.userId;
    //todo: validate
    let userCredits = req.body;
    //update userCredits
    let resp = await ds.saveUserCredits(
        userId,
        userCredits
    );
    res.status(200).json(userCredits);
});

/**
 * Update User Credits - single op
 * @route Patch /user/:userId/credits/add/:(allocated|remaining)/:creditType/:amount
 * @group User Credit Management - Operations for User Credit Management
 * @param {string} userId.query.required - userId
 * @param {string} allocatedOrRemaining.query.required - allocatedOrRemaining
 * @param {string} creditType.query.required - creditType
 * @param {number} amount.query.required - amount
 * @returns {string} 200 -
 * @returns {Error}  default - Unexpected error
 */
router.patch("/:userId/credits/add/:allocatedOrRemaining/:creditType/:amount", async (req, res, next) => {
    let userId = req.params.userId;
    let creditType = req.params.creditType;
    let allocatedOrRemaining = req.params.allocatedOrRemaining;
    let amount = parseInt(req.params.amount);
    if (isNaN(amount)) {
      throw "Amount is not a number";
    }
    if (!(['allocated','remaining'].includes(allocatedOrRemaining))) {
      throw "format must be ../add/remaining or ../add/allocated";
    }
    let currentCredits = await ds.getUserCredits(userId);
    let property = allocatedOrRemaining + "_" + creditType + "Credits";
    currentCredits[property] += amount;

    //update userCredits
    let resp = await ds.saveUserCredits(
        userId,
        currentCredits
    );
    res.status(200).json(currentCredits);
});


module.exports = router;
