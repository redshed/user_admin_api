const express = require("express");
const passport = require("passport");
const router = express.Router();
const _ = require("lodash");
const createReferral = require('../interfaces/referral').createReferral;
const findReferral = require('../interfaces/referral').findReferral;
const lookupReferral = require('../interfaces/referral').lookupReferral;
// const updateReferral = require('../interfaces/referral').updateReferral;
// const findUser = require('../interfaces/user').findUser;
const saveUser = require('../interfaces/user').saveUser;

/**
 * Create referral - body requires referralUserId - userId is optional
 * @route POST /referral
 * @group Referral Management - Operations for Referral management
 * @returns {object} 200 - Referral model
 * @returns {Error} 500 - Unexpected error
 * @security JWT
 */
router.post("/", async (req, res, next) => {
    let body = req.body;
    let referralUserId = body.referralUserId;
    let userId = null;
    if (body.hasOwnProperty("userId")) {
        userId = body.userId;
    }
    let ref = await createReferral( referralUserId, userId );
    res.status(200).json(ref);
});

/**
 * @typedef ReferralUpdateRequest
 * @property {string} refCode.required
 * @property {string} email.required - user email
 * @property {string} password.required
 * @property {string} phone - E.164 format
 * @property {string} name
 * @property {string} company
*/

/**
 * Update referral - body requires refcode / email and password
 * @route POST /referral/update
 * @param {ReferralUpdateRequest.model} ReferralUpdateRequest.model.required
 * @group Referral Management - Operations for Referral management
 * @returns {object} 200 - Referral model
 * @returns {Error} 500 - Not found / error
 * @security JWT
 */
router.post("/update", passport.authenticate('bearer', { session: false }),
    async (req, res, next) => {
    let body = req.body;
    let refCode = body.refCode;

    //lookup refcode and see if we have a user
    let referral = await findReferral( refCode );
    if (referral === null) {
        res.status(500).send("no referral found");
    }
    //save user - will handle new user
    let authDetail = {
        email: req.body.email,
        password: req.body.password,
        phone: req.body.phone
    };
    let userDetail = {};
    if (req.body.hasOwnProperty("name")) { userDetail.name = req.body.name }
    if (req.body.hasOwnProperty("company")) { userDetail.company = req.body.company }
    let userData = { authDetail: authDetail, userDetail: userDetail };
    let user = await saveUser(referral.userId, userData );
    res.status(200).json(user);
});

//lookup referral and return in format for the mobile app
router.get("/lookup/:refCode", passport.authenticate('bearer', { session: false }),
    async (req, res, next) => {
    //lookup referral and user
    let refCode = req.params.refCode;
    let lookup = await lookupReferral(refCode);
    res.status(200).json(lookup);
});

router.get("/send", async (req, res, next) => {
    //lookup and send
});



module.exports = router;
