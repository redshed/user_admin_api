const express = require("express");
const router = express.Router();
const listUsers = require("../interfaces/user").listUsers;

//list users
router.get("/", async (req, res, next) => {
  let userType = req.query.user_type;
  let userList = await listUsers(userType);
  //get users in contact list
  res.status(200).json(userList);
});
module.exports = router;
