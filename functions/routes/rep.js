const express = require("express");
const router = express.Router();
const _ = require("lodash");
const datastore = require("../modules/datastore").datastore;
const userModel = require("../interfaces/user");

//update rep
//update repDetail
router.patch("/:userId", async (req, res, next) => {
  let userId = req.params.userId;
  let admin = req.firebase_admin;
  let db = admin.firestore();
  let repDetail = req.body.repDetail;
  //TODO: validate body against model

  //check user is titlerep
  let user = await datastore.getUser(admin.auth, db, userId);
  if (user.userDetail.user_type === userModel.userTypes.TitleRep) {
    let r = await db
      .collection("repDetail")
      .doc(userId)
      .update(repDetail)
      .catch(err => {
        req.logger.error(err.message);
        res.status(500).send(err.message);
      });
    if (r !== undefined) {
      res.status(200).send("Rep " + userId + " updated");
    }
  } else {
    res.status(500).send("User " + userId + " is not a TitleRep");
  }
});

module.exports = router;
