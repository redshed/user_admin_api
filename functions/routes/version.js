const express = require("express");
const passport = require("passport");
const router = express.Router();

const package_json = require('../package.json');

/**
 * @route GET /version
 * @group Version
 * @returns {string} 200 - version number
 * @returns {Error}  Unexpected error
 */
router.get("/", passport.authenticate('bearer', { session: false }),
    async (req, res, next) => {
    res.status(200).json({ version: package_json.version });
});

module.exports= router;
