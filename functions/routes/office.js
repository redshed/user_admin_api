const express = require("express");
const router = express.Router();
const listOffices = require("../interfaces/office").listOffices;
const getRepUserCount = require("../interfaces/office").getRepUserCount;
const getOffice = require("../interfaces/office").getOffice;
const saveOffice = require("../interfaces/office").saveOffice;
const deleteOffice = require("../interfaces/office").deleteOffice;

//list all
router.get("/", async (req, res, next) => {
    let officeList = await listOffices();
    res.send(officeList);
});

//office CRUD
router.get("/:officeId", async (req, res, next) => {
    let officeId = req.params.officeId;
    let office = await getOffice(officeId);
    res.status(200).json(office);
});

router.post("/", async function(req, res, next) {
    let office = req.body;
    let officeResult = await saveOffice(office)
        .catch(err => {
            req.logger.error(err);
            res.status(500).send(err);
        });
    if (officeResult) {
        res.status(200).send(officeResult);
    }
});

router.patch("/:officeId", async function(req, res, next) {
    let office = req.body;
    let officeResult = await saveOffice(office)
        .catch(err => {
            req.logger.error(err);
            res.status(500).send(err);
        });
    if (officeResult) {
        res.status(200).send(officeResult);
    }
});

router.delete("/:officeId", async (req, res, next) => {
  let officeId = req.params.officeId;
  let r = await deleteOffice( officeId );
  res.status(200).send("office " + officeId + " deleted");
});


router.get("/RepUserCount/:officeId", async (req, res, next) => {
    let officeId = req.params.officeId;
    let count = await getRepUserCount( officeId );
    res.send(count);
});

module.exports = router;
