const ds = require('../modules/datastore').datastore;

const listCompanies = async () => {
    return await ds.listCompanies();
};

const saveCompany = async ( company ) => {
    return await ds.saveCompany( company );
};

const getCompany = async ( companyId ) => {
    return await ds.getCompany( companyId );
};

module.exports = { listCompanies, saveCompany, getCompany };

