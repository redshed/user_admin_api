const ds = require('../modules/datastore').datastore;

const generateRefCode = () => {
    let valid = false;
    let refCode = "";
    let validChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    while (!valid) {
        for (let i = 0; i < 6; i++)
            refCode += validChars.charAt(Math.floor(Math.random() * validChars.length));
        valid = ds.validateRefCode( refCode );
    }
    return refCode;

};

const createReferral = async ( referralUserId, userId ) => {
    //check for existing
    let referral = await ds.findReferral( referralUserId, userId) ;
    if (!referral) {
        referral = await ds.saveReferral( generateRefCode(), referralUserId, userId );
    }
    return referral;
};

const updateReferral = async ( refCode, userData ) => {
  let referral = await ds.getReferral( refCode );
  let userId = referral.userId;
  let user = await ds.getUser(userId);
  user = await ds.saveUser();
  return user;
};

const findReferral = async ( refCode ) => {
    return await ds.getReferral( refCode );
    //get extra info on offices etc
};

const mapReferraltoMobileResponse = ( user, rep, office, company ) => {
    let expected = {
        firstName: user.userDetail.firstName,
        lastName: user.userDetail.lastName,
        email: user.authDetail.email,
        phoneNumber: user.authDetail.phoneNumber,
        TitleRepLabel: rep.repDetail.repCode,
        AgentPhotoUrl: rep.userDetail.agentPhotoUrl,
        CustServNum: office.customerServicePhone,
        Office: office.name,
        OfficeDisabled: office.disabled,
        OfficeState: office.state,
        OrderTitleEmail: office.orderTitleEmail,
        OrderTitleEnabled: office.orderTitleEnabled,
        CopyRepOnOrderTitle: office.copyRepOnOrderTitle,
        CalculatorsEnabled: office.enabledProducts.calculators,
        FarmEnabled: office.enabledProducts.farmTools,
        WalkingFarmEnabled: office.enabledProducts.farmTools,
        ContainerUrl: company.containerUrl,
        SuppressTurnoverRate: company.suppressTurnoverRate,
        FarmSubscription: company.farmSubscription
    };
    return expected;
};

//returns data in the structure expected by the app
//checks the referral is valid and had a valid user connected
const lookupReferral = async ( refCode ) => {
    let referral = await ds.getReferral( refCode );
    let user = await ds.getUser( referral.userId );
    let rep = await ds.getUser( referral.referralUserId );
    let office = await ds.getOffice( rep.userDetail.officeId );
    let company = await ds.getCompany( office.companyId );
    return mapReferraltoMobileResponse( user, rep, office, company );
    //get extra info on offices etc
};




module.exports = {createReferral, findReferral, updateReferral, lookupReferral};
