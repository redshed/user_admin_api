const ds = require('../modules/datastore').datastore;

const listOffices = async () => {
    return await ds.listOffices();
};

const getRepUserCount = async ( officeId ) => {
    return await ds.getRepUserCount( officeId );
};

const getOffice = async ( officeId ) => {
    return await ds.getOffice( officeId );
};

const saveOffice = async ( office ) => {
    return await ds.saveOffice( office );
};

const deleteOffice = async ( office ) => {
    return await ds.deleteOffice( office );
};

module.exports = { listOffices, getRepUserCount, getOffice, saveOffice, deleteOffice };
