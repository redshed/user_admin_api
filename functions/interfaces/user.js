const _ = require("lodash");
const ds = require('../modules/datastore').datastore;
const UserTypes = require('../modules/datastore').UserTypes;


// const UserRequest = {
//     email:"",
//     phone:"",
//     password:"",
//     userDetail:{
//         user_type:"",
//         referral_user_id:"",
//         company_id:"",
//         office_id:"",
//         name:""
//     },
//     repDetail:{
//         repCode:"",
//         defaultProfileCredits:0,
//         defaultFarmCredits:0
//     },
//     isValid: ()=> { return false; }
// };

//these methods operate on the userDetail collection
//this allow us to add indexes / etc
//auth.user is really only used as a hook for identity and authentication
//the user "data" is in the details collecitons (userDetail, repDetail)


const findUser = async ( userId ) => {
    let user = await ds.getUser( userId );
    if ( user.userDetail.user_type === UserTypes.Realtor ) {
        // add in rep code
        user.rep = await ds.getUser( user.userDetail.referral_user_id );
    }
    return user;
};

//createUser CRUD, assumes userRequest has proper user structure
const saveUser = async ( userId, userRequest ) => {
    try {
        let authDetail = _.get(userRequest, "authDetail", undefined);
        let userDetail = _.get(userRequest, "userDetail", undefined);
        let repDetail = _.get(userRequest, "repDetail", undefined);
        let userCredits = _.get(userRequest, "userCredits", undefined);
        let user = await ds.saveUser(userId, authDetail, userDetail, repDetail, userCredits);
        return user;
    }
    catch (e) {
        throw e;
    }
};

const listUsers = async ( userType ) => {
    return await ds.listUsers( userType );
};

module.exports = { findUser, saveUser, listUsers };
