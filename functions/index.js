const package_json = require('./package.json');
const version = package_json.version;
const localPort = 5000;

const env = process.env.NODE_ENV;

const functions = require("firebase-functions");

//logging
//environment and logging
//mostly moved to detail_logger
const detailLogger = require('./detail_logger.js');
let logger=detailLogger.logger;

const config_result = require('dotenv').config();
if (config_result) {
    logger.info("loaded config");
}
//load / check env vars
let jwtSecret = process.env.JWT_SECRET;

//express and middleware
const express = require("express");
const cors = require("cors")({ origin: true });
const bodyParser = require("body-parser");


//routes
const version_route = require("./routes/version");
const users = require("./routes/users");
const user = require("./routes/user");
const company = require("./routes/company");
const office = require("./routes/office");
const rep = require("./routes/rep");
const referral = require("./routes/referral");

console.log("starting");
/* Express */
const app = express();

//add middlewares
app.use(cors);
app.use(bodyParser.json());

//inject logger to request
//probably redundant
app.logger = logger;
app.use((req, res, next) => {
  req.logger = logger;
  //logger.info(req);
  next();
});

// //firebase functions:config:set jwt.secret="redshedsecret"
// try {
//     jwtSecret = functions.config().jwt.secret;
//     logger.info("loaded jwt.secret from firebase config");
// }
// catch (e) {
//     logger.info("failed to load jwt.secret from firebase config");
// }
// const jwt = require('express-jwt');
// app.use(jwt({secret: jwtSecret}));
//SET UP PASSPORT BEARER TOKEN STRATEGY
const passport = require('passport');
const tokenStrategy = require('passport-http-bearer').Strategy;
const defaultUser = { username: "api@pforceapp.com", token: "d559516d23834fc59760797b6a2299b4"};
passport.use(new tokenStrategy(
    function(token, cb) {
        let user = defaultUser;
        if (token !== defaultUser.token) {
            return cb(null, false);
        }
        else {
            return cb(null, user);
        }
    }));

//add routes
app.use("/version", version_route);
app.use("/users", users);
app.use("/user", user);
app.use("/company", company);
app.use("/office", office);
app.use("/rep", rep);
app.use("/referral", referral);

if (true) {
    const expressSwagger = require('express-swagger-generator')(app);
    let swaggerHost = "localhost";
    let swaggerSchemes = ['https', 'http'];
    // if (swaggerHost === "localhost") {
    //     swaggerSchemes = ['http'];
    // }
    let options = {
        swaggerDefinition: {
            info: {
                description: 'Admin User Data Service',
                title: 'User Data Service',
                version: version,
            },
            host: swaggerHost + ":" + localPort,
            basePath: '',
            produces: [
                "application/json"
            ],
            schemes: swaggerSchemes,
            securityDefinitions: {
                JWT: {
                    type: 'apiKey',
                    in: 'header',
                    name: 'Authorization',
                    description: "",
                }
            }
        },
        basedir: __dirname, //app absolute path
        files: ['./routes/**/*.js'] //Path to the API handle folder
    };
    expressSwagger(options);
}

logger.info("NODE_ENV = " + env);
logger.info("version " + version);

let local = (process.env.hasOwnProperty("RUN_LOCAL") && process.env.RUN_LOCAL);
if (local) {
    //for local debug
    app.listen(localPort);
    logger.info("running local");
}
else {
    //for firebase functions
    logger.info("exporting api (firebase)");
    exports.api = functions.https.onRequest(app);
}



